const express = require('express');
const fs = require('fs');
const router = express.Router();
const folderPath = './messages';

const data = [];

router.get('/', async (req, res) => {
    try {
        fs.readdir(folderPath, (err, files) => {
            files.forEach(file => {
                const fileName = folderPath + '/' + file;
                data.push(JSON.parse(fs.readFileSync(fileName)));
            });

        });
        res.send(data.slice(-5));
    } catch (e) {
        console.log(e);
    }
});


router.post('/', async (req, res) => {
    try {
        const message = req.body;
        message.date = new Date().toISOString();
        const fileName = `./messages/${message.date}.txt`;
        fs.writeFileSync(fileName, JSON.stringify(message));
        res.send({message: 'File was saved'})
    } catch (e) {
        console.log(e);
    }
});

module.exports = router;